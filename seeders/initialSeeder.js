const mongoose = require("mongoose");
const {faker} = require("@faker-js/faker");
const userModel = require("../src/models/user");
const todoModel = require("../src/models/todo");
mongoose.connect("mongodb://localhost:27017/unifiSolution", {useNewUrlParser: true});

let users = [{
    firstName: 'Ahmad',
    lastName: 'Matar',
    username: 'matar98',
    password: 'password'
}, {
    firstName: 'Mohammad',
    lastName: 'Ahmad',
    username: 'mohammad99',
    password: '123456789'
}, {
    firstName: 'Hussein',
    lastName: 'Omar',
    username: 'zeroo1234',
    password: '1234512345'
}];

async function seed() {
    for (let i = 0 ; i < users.length; i++){
        let user = await userModel.create(users[i]);
        await todoModel.create({userId:user._id,text:faker.git.commitMessage()})
    }
    console.log('done');
    process.exit();
}

seed();

