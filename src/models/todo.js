const {Schema, model, ObjectId} = require("mongoose");

const todoSchema = new Schema({
    text: String,
    userId: {
        type: ObjectId,
        required: true
    },
},{
    versionKey:false
});

let Todo = model('Todo', todoSchema);
module.exports = Todo;
