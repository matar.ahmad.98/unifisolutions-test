const jsonwebtoken = require('jsonwebtoken');
const {Schema, model} = require("mongoose");

const userSchema = new Schema({
    firstName: {
        type: String,
        required: 'Your firstName is required'
    },
    lastName: {
        type: String,
        required: 'Your lastName is required'
    },
    username: {
        type: String,
        unique: true,
        required: 'Your password is required',
        max: 20,
        min: 4
    },
    password: {
        type: String,
        required: 'Your password is required',
        max: 20,
        min: 8
    }
});

userSchema.methods.comparePassword = function (password) {
    return this.password === password;
};

userSchema.methods.generateJWT = function () {
    const _id = this._id;
    const expiresIn = '1d';
    const payload = {
        id: _id,
        iat: Date.now()
    };
    const signedToken = jsonwebtoken.sign(payload, process.env.JWT_SECRET, {expiresIn: expiresIn});
    return "Bearer " + signedToken;
};

let User = model('User', userSchema);

module.exports = User;
