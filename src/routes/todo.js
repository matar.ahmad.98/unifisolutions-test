let router = require('express').Router();
let todoController = require('../controllers/todo');
const {APIHandler} = require("../helpers/APIHandler");
const {createOrUpdateTodoValidators} = require("../helpers/Validator");
const validate = require("../middlewares/validate");

router.get('/one/:id', APIHandler(todoController.getOne));
router.post('/',createOrUpdateTodoValidators,validate, APIHandler(todoController.create));
router.put('/:id',createOrUpdateTodoValidators,validate, APIHandler(todoController.update));
router.delete('/:id', APIHandler(todoController.delete));
module.exports = router;
