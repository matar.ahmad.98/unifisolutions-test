const router = require('express').Router();
const auth = require("../middlewares/authenticate");


router.use('/user', require('./user'));
router.use('/todo',auth, require('./todo'));

module.exports = router;
