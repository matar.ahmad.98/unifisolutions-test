let router = require('express').Router();
let userController = require('../controllers/user');
const validate = require("../middlewares/validate");
const {APIHandler} = require("../helpers/APIHandler");
const {signInValidators} = require("../helpers/Validator");


/* GET users listing. */
router.post('/signIn', signInValidators, validate, APIHandler(userController.signIn));
module.exports = router;
