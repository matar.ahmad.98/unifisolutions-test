const passport = require("passport");
const {responseErrorSender} = require("../helpers/APIResponse");

module.exports = (req, res, next) => {
    passport.authenticate('jwt', function(err, user) {
        if (err) return next(err);

        if (!user) return responseErrorSender(res,"Unauthorized Access - No Token Provided!",null,401);
        req.user = user;

        next();

    })(req, res, next);
};