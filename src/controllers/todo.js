let todoModel = require('../models/todo');
let {responseSender} = require('../helpers/APIResponse');


exports.getOne = async (req, res) => {
    let todo = await todoModel.findById(req.params.id).where('userId').equals(req.user._id).exec();
    return responseSender(res, todo);
};
exports.create = async (req, res) => {
    let todo = await todoModel.create({text: req.body.text, userId: req.user._id});
    return responseSender(res, todo)
};
exports.update = async (req, res) => {
    let todo = await todoModel.findOneAndUpdate({userId:req.user._id,_id:req.params.id},{text: req.body.text}).exec();
    if (todo)
        return responseSender(res);
    return responseSender(res,null,"No todo with this id!")
};
exports.delete = async (req, res) => {
    let todo = await todoModel.deleteOne({userId:req.user._id,_id:req.params.id}).exec();
    if (todo.deletedCount > 0)
        return responseSender(res,todo);
    return responseSender(res,null,"No todo with this id!")
};