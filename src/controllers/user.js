let userModel = require('../models/user');
let {responseSender,responseErrorSender} = require('../helpers/APIResponse');

exports.signIn = async (req,res) => {
    let user = await userModel.findOne({username:req.body.username}).exec();
    console.log(req.body.username);
    if (!user)
         return responseErrorSender(res,"there is no user with this username",null,401);
    if (!user.comparePassword(req.body.password)){
        return responseErrorSender(res,"wrong password",401);
    }
    let token = user.generateJWT();
     return responseSender(res,{token,user})
};