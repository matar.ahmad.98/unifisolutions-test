const {check} = require('express-validator');
exports.signInValidators = [
    check('password').exists().withMessage('is required').bail().isLength({min: 8}).withMessage('Must be at least 8 chars long').isLength({max: 20}).withMessage("Must be at most 20 chars long"),
    check('username').exists().withMessage('is required').bail().isLength({min: 4}).withMessage('Must be at least 4 chars long').isLength({max: 20}).withMessage("Must be at most 20 chars long"),
];

exports.createOrUpdateTodoValidators = [
    check('text').exists().withMessage('is required')
];