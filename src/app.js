let express = require('express');
let logger = require('morgan');
const mongoose = require("mongoose");
const passport = require("passport");
let indexRouter = require('./routes');
let {ServerErrorHandler} = require('./helpers/APIResponse');
require('dotenv').config();

mongoose.connect("mongodb://localhost:27017/unifiSolution", {useNewUrlParser: true});

let app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

require("./middlewares/jwt")(passport);
app.use(passport.initialize());

app.use('/', indexRouter);
app.use(ServerErrorHandler);

app.listen(process.env.PORT||5000, () => {
    console.log("Server has started on port " + 5000);
});

module.exports = app;